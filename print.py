#!/usr/bin/env python

import fontforge
import sys

font = sys.argv[1]

for glyph in fontforge.open(font).glyphs():
    glyph.export("png/" +glyph.glyphname + ".png", 600)

fontforge.open(font).generate('ocr-pbi.ttf')
